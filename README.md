Quick explanation:

Basically native (iOS, Android) will listen for notification message  (via an TCP connection). 
When ever an message arrived, it will be send to React Native by the Event Emitter (built in React native bridge).
When React Native received notification, it will trigger app notification by calling an native module.

The flow: Native -> React Native -> Native.

I'm using Firebase for setting up the push notification. It supported for both Android and iOS.


iOS Demo: https://www.youtube.com/watch?v=GyNmSFKoauI&feature=youtu.be

Android demo: https://www.youtube.com/watch?v=AL7irAiuJCA&feature=youtu.be

Push notification demo (Android): https://www.youtube.com/watch?v=A4SK242lyaU&feature=youtu.be