/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
  ListView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { Platform } from 'react-native';
import FCM, {FCMEvent} from "react-native-fcm";

export const QUESTIONS = [
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  'Morbi faucibus maximus nulla, sed posuere elit faucibus scelerisque.',
  'In hac habitasse platea dictumst. Pellentesque suscipit est vitae nisi vulputate lobortis.',
  'Proin pretium ante eget pellentesque luctus. Cras et scelerisque urna. Pellentesque non quam purus. Duis euismod risus ut laoreet ultrices. Ut ut urna arcu.',
  'Suspendisse vitae consequat tortor. Donec quis enim arcu. Aenean id mauris porttitor, consequat dolor sit amet, porttitor nisl. Nunc vel nisl varius, lacinia risus non, interdum sem.',
  'Suspendisse vitae consequat tortor. Donec quis enim arcu. Aenean id mauris porttitor, consequat dolor sit amet, porttitor nisl. Nunc vel nisl varius, lacinia risus non, interdum sem.',
  'Suspendisse vitae consequat tortor. Donec quis enim arcu. Aenean id mauris porttitor, consequat dolor sit amet, porttitor nisl. Nunc vel nisl varius, lacinia risus non, interdum sem.',
  'Suspendisse vitae consequat tortor. Donec quis enim arcu. Aenean id mauris porttitor, consequat dolor sit amet, porttitor nisl. Nunc vel nisl varius, lacinia risus non, interdum sem.',
  'Suspendisse vitae consequat tortor. Donec quis enim arcu. Aenean id mauris porttitor, consequat dolor sit amet, porttitor nisl. Nunc vel nisl varius, lacinia risus non, interdum sem.',
  'Suspendisse vitae consequat tortor. Donec quis enim arcu. Aenean id mauris porttitor, consequat dolor sit amet, porttitor nisl. Nunc vel nisl varius, lacinia risus non, interdum sem.',
  'Proin pretium ante eget pellentesque luctus. Cras et scelerisque urna. Pellentesque non quam purus. Duis euismod risus ut laoreet ultrices. Ut ut urna arcu.',
  'Proin pretium ante eget pellentesque luctus. Cras et scelerisque urna. Pellentesque non quam purus. Duis euismod risus ut laoreet ultrices. Ut ut urna arcu.',
];


export default class App extends Component {
  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(QUESTIONS),
    };
  }

  componentDidMount() {
    this.notificationListener = FCM.on(FCMEvent.Notification, notif => {
      console.log("Notification", notif);
      if (notif.local_notification) {
        return;
      }
      if (notif.opened_from_tray) {
        return;
      }

      if (Platform.OS === 'ios') {
        //TODO
      } else {
        const {fcm} = notif;
        const {custom_notification} = notif;
        FCM.presentLocalNotification({
          vibrate: 500,
          title: fcm.title,
          body: fcm.body,
          click_action: custom_notification.action,
          priority: "high",
          show_in_foreground: true,
          picture: 'https://firebase.google.com/_static/af7ae4b3fc/images/firebase/lockup.png'
        });
      }
    })

  }

  componentWillUnmount() {
    this.notificationListener.remove();
  }

  renderQuestionRow(question, sectionId, index) {
    return (
      <View style={{paddingHorizontal: 12, paddingBottom: 8, borderBottomWidth: 1, borderColor: 'rgba(0, 0, 0, 0.05)'}}>
        <Text style={{color: 'black'}}>{'Question ' + index + ':'}</Text>
        <Text numberOfLines={3}>{question}</Text>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderQuestionRow}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
